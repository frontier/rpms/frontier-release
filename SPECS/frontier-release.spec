#
# frontier-release :   RPM Spec file for package describing the
#       frontier yum repository
# Author: Dave Dykstra <dwd@fnal.gov>
# Creation Date: 18 December 2012
#

Summary: CERN Frontier yum repository configuration
Name: frontier-release
Version: 1.3
Release: 1
License: GPL
Group: System/Server

Source0: RPM-GPG-KEY-cern-frontier
Source1: cern-frontier.repo
Source2: cern-frontier-debug.repo

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This package contains the CERN Frontier repository configuration for yum.

%prep

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg
install -pm 644 %{SOURCE0} $RPM_BUILD_ROOT%{_sysconfdir}/pki/rpm-gpg

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d



%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%config(noreplace) /etc/yum.repos.d/*
/etc/pki/rpm-gpg/*

%changelog
* Wed Aug 28 2024 Carl Vuosalo <carl.vuosalo@cern.ch> - 1.3-1
- Change from using http to https, which is now supported by
   frontier.cern.ch.

* Wed Aug 23 2023 Carl Vuosalo <carl.vuosalo@cern.ch> - 1.2-1
- Make repos specific to OS version: el7, el8, el9, and possibly
   others in the future. Each has a corresponding "-debug" repo.

* Fri Aug 29 2014 Dave Dykstra <dwd@fnal.gov> - 1.1-1
- Add cern-frontier-debug repo, disabled by default

* Wed Dec 19 2012 Dave Dykstra <dwd@fnal.gov> - 1.0-1
- Initial creation

